export default ({
  activeDonation: (focus, donationImage) => ({
    type: 'STATE',
    state: {
      itemFocus: focus,
      itemGrew: true,
      bannerImage: donationImage
    }
  })
})
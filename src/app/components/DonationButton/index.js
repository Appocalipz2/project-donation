import React from 'react'
import PropTypes from 'prop-types'
import ClassNames from 'classnames'
import Link from '../../core-components/Link'
import actions from './actions'
import style from './styles.scss'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import history from '../../core-components/History/history'
import ProgressBar from 'react-progressbar.js'
const Circle = ProgressBar.Circle

class DonationButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total: 0
    };
  }

  render () {
    return (
      <div className={style.donationContainer}>
        <div className={style.donationInnerContainer}>
          <div className={style.title}>Donation</div>
          <input onChange={(e) => { this.setState({total: e.target.value}) }} type={"number"} className={style.input} placeholder={"R 0.00"} />
          <div className={style.donateButtonContainer}>
            <div className={style.donateButton} onClick={() => {this.props.donate(this.state.total)}}>
              Donate
            </div>
            <div className={style.cancel} onClick={() => {this.props.cancel()}}>
              Cancel
            </div>
          </div>
        </div>
      </div>
    )
  }
}

DonationButton.stateToProps = (state) => ({})

DonationButton.dispatchToProps = (dispatch) => bindActionCreators(actions, dispatch)

DonationButton.propTypes = {
  cancel: PropTypes.func.isRequired,
  donate: PropTypes.func.isRequired
};

export default connect(DonationButton.stateToProps, DonationButton.dispatchToProps)(DonationButton)




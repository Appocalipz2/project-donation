import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import Link from '../../core-components/Link';
import Actions from './actions';
import style from './styles.scss';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import history from '../../core-components/History/history';
import Swipeable from 'react-swipeable'
import mainImage from '../../../www/img/main.jpg'
import moment from 'moment';

class BannerSection extends React.Component {

  // Reset the body position after a swipe ended
  swiped(e, absX){
    document.body.style.position = "relative";
    document.body.style.left = `0px`;
  }

  // Allows the user to swipe right over the banner to go to the home page
  swipingRight(e, absX) {
    document.body.style.position = "relative";
    document.body.style.left = `${absX}px`;
    if (absX > 105) {
      history.push("/");
      document.body.style.left = `0px`;
    }
  }

  // Allows the user to swipe left over the banner to go to the home page
  swipingLeft(e, absX) {
    this.bodyTransition(document.body, true)
    document.body.style.position = "relative";
    document.body.style.left = `-${absX}px`;
    if (absX > 105) {
      history.push("/");
      document.body.style.left = `0px`;
    }
  }

  formatValue (num) {
    var stringSplit = num.toString().split('.');
    if (stringSplit[0].length >= 5) stringSplit[0] = stringSplit[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
    if (stringSplit[1] && stringSplit[1].length >= 5) stringSplit[1] = stringSplit[1].replace(/(\d{3})/g, '$1 ');
    return stringSplit.join('.');
  }

  dayDiff (first, second) {
    return Math.round((second-first)/(1000 * 60 * 60 * 24));
  }

  minDiff (first, second) {
    return Math.floor((first - second) / 60000)
  }

  getTime(timeA, timeB, time, text) {
    let totalTime = ""
    var days = this.dayDiff(moment(timeA, "DD-MM-YYYY").toDate().getTime(), timeB)
    if (days === 0) {
      var minutes = this.minDiff(moment(timeA, "YYYY-MM-DD'T'HH:mm:SS'Z'").toDate().getTime(), timeB)
      if (minutes >= 60) totalTime = `${Math.abs(minutes / 60)} hours ago`
      else totalTime = `${(time ? `${Math.abs(minutes)} ` : "")}${(text ? "minutes ago" : "")}`
    }
    else totalTime = `${(time ? `${Math.abs(days)} ` : "")}${(text ? "days ago" : "")}`
    return totalTime
  }

  render() {
    var topSection = (!this.props.itemFocus ? (
      <div className={`${(this.props.itemGrew ? style.bannerScreenTitleFadeIn : "")} ${style.bannerScreenTitle}`}>
        Featured Charity
      </div>
    ): "")

    const styleState = () => (this.props.itemGrew ? (this.props.itemFocus ? `${style.bannerFull}` : `${style.bannerSmall}`) : (this.props.itemFocus ? `${style.bannerFull}` : ""))
    const styleStateFull = () => (this.props.itemGrew ? (this.props.itemFocus ? `${style.bannerImagePlaceholderHide}` : ``) : (this.props.itemFocus ? `${style.bannerImagePlaceholderHide}` : ""))

    var totalContributors = 0
    var total = 0
    var totalPercentage = 0

    if (typeof this.props.activeDonation.funding !== 'undefined') {
      this.props.activeDonation.funding.map(item => {
        totalContributors += 1
        total += item.donation
      })
      totalPercentage = Math.ceil((total / this.props.activeDonation.raising) * 100)
    }



    let extraContent = null
    if (this.props.itemFocus) {
      extraContent = (
        <div className={style.screenBlackOut}>
          <div className={style.companyImage} style={{backgroundImage: `url('/assets/images/donations/${this.props.activeDonation.bannerSmall}')`}}></div>
          <div className={style.title}>{(typeof this.props.activeDonation.title !== 'undefined' ? this.props.activeDonation.title : "")}</div>
          <div className={style.percentageBar}>
            <div className={style.percentageBarInner} style={{width: `${(totalPercentage > 100.00 ? 100.00 : totalPercentage)}%`}}>
              <div className={style.percentageBarPopup}>
                {totalPercentage}%
              </div>
            </div>
          </div>
          <div className={style.goal}>
            <div className={style.text}>R {(typeof this.props.activeDonation.raising !== 'undefined' ? this.formatValue(this.props.activeDonation.raising) : "0.00")}</div>
            <div className={style.subTitle}>Goal</div>
          </div>
          <div className={style.days}>
            <div className={style.text}>{(typeof this.props.activeDonation.completeDate === 'undefined' ? "0" : this.getTime(this.props.activeDonation.completeDate, (new Date()).getTime(), true, false))}</div>
            <div className={style.subTitle}>{(typeof this.props.activeDonation.completeDate === 'undefined' ? "days" : this.getTime(this.props.activeDonation.completeDate, (new Date()).getTime(), false, true))}</div>
          </div>
          <div className={style.contributors}>
            <div className={style.text}>{totalContributors}</div>
            <div className={style.subTitle}>Contributors</div>
          </div>
        </div>
      )
    }

    return (
      <div className={style.bannerScreenContainer}>
        <Swipeable
          onSwiped={(a,b) => this.swiped(a,b)}
          onSwipingRight={(a,b) => this.swipingRight(a,b)}
          onSwipingLeft={(a,b) => this.swipingLeft(a,b)}>
          <div className={style.bannerScreenInnerContainer}>
            {topSection}
            <div className={style.bannerImageContainer}>
              <div style={{backgroundImage: `url('${this.props.bannerImage}')`}} className={`${style.bannerImage} ${styleState()}`}>
                {extraContent}
              </div>
              <div style={{backgroundImage: `url('${mainImage}')`}} className={`${style.bannerImagePlaceholder} ${styleState()} ${styleStateFull()}`}></div>
            </div>
          </div>
        </Swipeable>
      </div>
    )
  }
}

BannerSection.stateToProps = (state) => ({
  itemFocus: state.itemFocus,
  itemGrew: state.itemGrew,
  bannerImage: state.bannerImage,
  activeDonation: state.activeDonation
})

BannerSection.dispatchToProps = (dispatch) => bindActionCreators(Actions, dispatch)

BannerSection.propTypes = {};

export default connect(BannerSection.stateToProps, BannerSection.dispatchToProps)(BannerSection)

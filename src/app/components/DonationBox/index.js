import React from 'react'
import PropTypes from 'prop-types'
import ClassNames from 'classnames'
import Link from '../../core-components/Link'
import actions from './actions'
import style from './styles.scss'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import history from '../../core-components/History/history'
import ProgressBar from 'react-progressbar.js'
const Circle = ProgressBar.Circle

class DonationBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const donateButton =
      <div className={`${(this.props.donationButtonActive ? style.donateButtonGrow : "")} ${style.donateAgain}`}>
        <div className={style.donateAgainInnerContainer}>
          <div className={style.smiley} />
          Donate Again
        </div>
      </div>

    return (
      <div className={style.donationBoxContainer} onClick={() => {this.props.onClick()}}>
        {donateButton}
        <div className={`${style.donationBoxInnerContainer} ${(this.props.donationButtonActive ? style.donateButtonGrowMainContainerShrink : "")}`}>
          <div className={style.donationBoxStatus}>
            <Circle progress={(this.props.percentageRaised / 100 > 1 ? 1 : this.props.percentageRaised / 100)}
                    initialAnimate={true}
                    options={{strokeWidth: 7.5, color: "#00ad0e"}}
                    containerClassName={style.graph} ></Circle>
            <Circle progress={1}
                    initialAnimate={false}
                    options={{strokeWidth: 7.5, color: "#eaeaea"}}
                    containerClassName={style.filledGraph} ></Circle>
            <div className={style.donationBoxImage} style={{backgroundImage: `url('/assets/images/donations/${this.props.bannerSmall}')`}}></div>
          </div>

          <div className={style.donationBoxInformation}>
            <div className={style.donationBoxTitle}>
              {this.props.title}
            </div>
            <div className={style.donationBoxSubTitle}>
              {this.props.subTitle}
            </div>
            <div className={style.donationBoxSubInfoContainer}>
              <div className={style.donationBoxCompany}>
                {this.props.company},
              </div>
              <div className={style.donationBoxLocation}>
                {this.props.location}
              </div>
            </div>
          </div>
          <div className={style.donationButtons}>
            <div className={style.donationRemainingDays}>
              {this.props.remainingDays} Days Remaining
            </div>
            <div className={style.donationRaised}>
              {this.props.percentageRaised}% Raised
            </div>
          </div>
        </div>
      </div>
    )
  }
}

DonationBox.stateToProps = (state) => ({
  userId: state.userId
})

DonationBox.dispatchToProps = (dispatch) => bindActionCreators(actions, dispatch)

DonationBox.propTypes = {
  donationButtonActive: PropTypes.bool,
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
  company: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
  remainingDays: PropTypes.number.isRequired,
  percentageRaised: PropTypes.number.isRequired,
  banner: PropTypes.string.isRequired,
  bannerSmall: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
};

export default connect(DonationBox.stateToProps, DonationBox.dispatchToProps)(DonationBox)

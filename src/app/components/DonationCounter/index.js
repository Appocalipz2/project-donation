import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import Link from '../../core-components/Link';
import actions from './actions';
import style from './styles.scss';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

class DonationCounter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      donations: this.props.donations
    };
  }

  render() {
    return (
      <div className={style.donationCounterContainer}>
        <span className={style.donationCounter}>
          {
            this.props.donations.toString().split("").map((number, e) => {
              return <div key={e} className={style.donationCounterNumber}>{number}</div>
            })
          }
        </span>
        <div className={style.donationTitle}>Donations</div>
      </div>
    )
  }
}

DonationCounter.stateToProps = (state) => ({})

DonationCounter.dispatchToProps = (dispatch) => bindActionCreators(actions, dispatch)

DonationCounter.propTypes = {
  donations: PropTypes.number.isRequired,
  store: PropTypes.object
};

export default connect(DonationCounter.stateToProps, DonationCounter.dispatchToProps)(DonationCounter)

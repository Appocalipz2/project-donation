import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import Link from '../../core-components/Link';
import actions from './actions';
import style from './styles.scss';
import fire from '../../core-components/Fire/fire';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import DonationCounter from '../DonationCounter';
import Swipeable from 'react-swipeable'

class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fireBaseTotalDonations: fire.database().ref('count')
    };
  }

  componentWillMount (){
    // Reset fireBase listeners
    this.state.fireBaseTotalDonations.off()

    // Results
    this.state.fireBaseTotalDonations.on('child_added', snapshot => {
      if (snapshot.key === "donations") {
        this.props.setDonations(snapshot.val())
      }
    })

    // Changes
    this.state.fireBaseTotalDonations.on('child_changed', snapshot => {
      if (snapshot.key === "donations") {
        this.props.setDonations(snapshot.val())
      }
    })
  }

  render() {
    return (
      <div className={`${(this.props.itemFocus ? style.navBarContainerHidden : style.navBarContainerFadeIn)} ${style.navBarContainer}`}>
        <DonationCounter store={this.props.store} donations={this.props.donations}></DonationCounter>
      </div>
    )
  }
}

TopBar.stateToProps = (state) => ({
  itemFocus: state.itemFocus,
  donations: state.donations
})

TopBar.dispatchToProps = (dispatch) => bindActionCreators(actions, dispatch)

TopBar.propTypes = {
  store: PropTypes.object
};

export default connect(TopBar.stateToProps, TopBar.dispatchToProps)(TopBar)

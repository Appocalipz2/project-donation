export default ({
  setDonations: (value) => ({
    type: 'STATE',
    state: {
      donations: value
    }
  })
})
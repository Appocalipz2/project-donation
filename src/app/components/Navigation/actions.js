export default ({
  newFilter: (value) => ({
    type: 'STATE',
    state: {
      filter: value
    }
  }),
  activeTab: (value) => ({
    type: 'STATE',
    state: {
      activeTab: value
    }
  })
})
import React from 'react';
import PropTypes from 'prop-types';
import ClassNames from 'classnames';
import Link from '../../core-components/Link';
import Actions from './actions';
import style from './styles.scss';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

const routes = require('./../../../resources/routes.json').default;

class Navigation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstActive: true
    };
  }

  getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  render() {
    var navigationItems = routes
      .filter(item => {
        if (!this.props.itemFocus)
          return (item.navBarTitle && item.group === "1")
        else
          return (item.navBarTitle && item.group === "2")
      })
      .map(item => [item.navBarTitle, item.path, item])
      .map((tab, i) => {
        return <Link onClick={() => {
            if(tab[2].group === "2") {
              this.props.activeTab(i)
            }
            var param = this.getParameterByName("filter", tab[2].path)
            if (typeof param !== 'undefined' && param !== '' && param !== null) {
              this.props.newFilter(param)
            }
            return (this.state.firstActive ? this.setState({firstActive: false}) : "")
          }}
          className={`${((this.state.firstActive && i === 0) ? style.activeTab : "")} ${style.navigationItem} ${(this.props.itemFocus ? style.searchTabShrinking : (this.props.itemGrew ? style.searchTabGrowing : ""))}`}
          to={`${tab[1]}`}
          key={i}>
            {tab[0]}
        </Link>
      }
    )
    return (
      <div className={style.navigationContainer}>
        {navigationItems}
        <Link className={`${style.navigationSearchItem} ${(this.props.itemFocus ? style.searchTabShrink : (this.props.itemGrew ? style.searchTabGrow : ""))}`} to={"/search"} key={"/search"}></Link>
      </div>
    )
  }
}

Navigation.stateToProps = (state) => ({
  activeTab: state.activeTab,
  itemFocus: state.itemFocus,
  itemGrew: state.itemGrew
})

Navigation.dispatchToProps = (dispatch) => bindActionCreators(Actions, dispatch)

Navigation.propTypes = {};

export default connect(Navigation.stateToProps, Navigation.dispatchToProps)(Navigation)







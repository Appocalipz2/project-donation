export default ({
  reset: (focus, orignalBannerImage) => ({
    type: 'STATE',
    state: {
      itemFocus: focus,
      itemGrew: true,
      bannerImage: orignalBannerImage
    }
  }),

  activeDonation: (focus, donationImage) => ({
    type: 'STATE',
    state: {
      itemFocus: focus,
      itemGrew: true,
      bannerImage: donationImage
    }
  })
})
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actions from './actions'
import fire from '../../core-components/Fire/fire';
import style from './styles.scss'
import DonationBox from '../../components/DonationBox';
import history from '../../core-components/History/history';
const request = require('request');
const moment = require('moment');

// Static reference to the donations database field with a orderBy
// This connection needs to be static / destroyable and not interfere with any state or props
var initialFireBaseDatabaseConnect = fire.database().ref('donations').startAt().orderByChild("created").limitToLast(100)

class Results extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // List of all the donation boxes populated by the DB
      donationBoxes: []
    }
  }

  fireBaseListenersInit () {
    // Initially the news has to be reset and not use the state values
    // By using the state values every time you will create doubles. This prevents that
    var news = false

    // Remove all old connections
    initialFireBaseDatabaseConnect.off()

    // Add a child to the donationBoxes if a new value is added to the database
    initialFireBaseDatabaseConnect.on('child_added', snapshot => {
      let oldBoxes = (!news ? [] : this.state.donationBoxes)
      news = true;
      oldBoxes.push(snapshot.val())
      this.setState({donationBoxes: oldBoxes})
    })

    // Update a child in the donationBoxes if a new value is added to the database
    initialFireBaseDatabaseConnect.on('child_changed', snapshot => {
      let boxes = this.state.donationBoxes.reverse().map(box => (box.id.toString() === snapshot.val().id.toString() ? snapshot.val() : box))
      this.setState({ donationBoxes: boxes })
    })
  }

  componentDidMount () {
    this.fireBaseListenersInit()
    // If the item is being focused. (For example you clicked on a donation) the banner image must change
    if (!this.props.itemFocus) {
      this.props.reset(false, this.props.originalBannerImage)
    }
  }

  componentWillReceiveProps (){
    // Remove all old connections
    initialFireBaseDatabaseConnect.off()

    // Set a new database connection with a new filter
    initialFireBaseDatabaseConnect = fire.database().ref('donations').orderByChild(this.props.filter).limitToLast(100)

    // Reset the donationBoxes
    this.setState({ donationBoxes: [] })
    this.fireBaseListenersInit()
  }

  // When a donation is clicked on.
  donationBoxVisit (id, banner) {
    // Remove all old connections
    initialFireBaseDatabaseConnect.off()

    // Set the new banner image
    this.props.activeDonation(
      true, banner
    )

    // Goto the donation page
    history.push("/donation?id=" + id);
  }

  // Difference between days
  daydiff (first, second) {
    return Math.round((second-first)/(1000*60*60*24));
  }

  // Format long values with spaces after every third value
  formatValue (num) {
    var __toStringSplit = num.toString().split('.');
    if (__toStringSplit[0].length >= 5) __toStringSplit[0] = __toStringSplit[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
    if (__toStringSplit[1] && __toStringSplit[1].length >= 5) __toStringSplit[1] = __toStringSplit[1].replace(/(\d{3})/g, '$1 ');
    return __toStringSplit.join('.');
  }


  render() {
    const boxes = this.state.donationBoxes.reverse().map((box, i) => {
      const totalFunded = (typeof box.funding === 'undefined' ? [] : box.funding).reduce((a, b) => a + b.donation, 0)
      const completeByDate =  moment(box.completeDate, 'DD-MM-YYYY').subtract(0, 'hours').format('MMM DD')
      const daysToGo = this.daydiff((new Date()).getTime(), moment(box.completeDate, 'DD-MM-YYYY').subtract(2, 'hours').toDate().getTime())
      const youFunded = (typeof box.funding === 'undefined' ? [] : box.funding).filter(fund => fund.userId === this.props.userId).length

      return <DonationBox onClick={() => {this.donationBoxVisit(box.id, `/assets/images/donations/${box.banner}`)}}
                   key={i}
                   donationButtonActive={youFunded > 0}
                   title={box.title}
                   subTitle={`RAISING R ${this.formatValue(box.raising)} BY ${completeByDate.toUpperCase()}`}
                   company={box.company}
                   location={box.location}
                   remainingDays={daysToGo}
                   percentageRaised={Math.ceil((totalFunded / box.raising) * 100)}
                   banner={`/assets/images/donations/${box.banner}`}
                   bannerSmall={box.bannerSmall}></DonationBox>
    })

    // If no boxes have been loaded then a loading bar is displayed waiting for the database to return results
    if (boxes.length <= 0) {
      return (
        <div>
          <div className={style.loadingContent}>
            <div className={style.loadingContentText}>
              Loading Feed ...
            </div>
          </div>
        </div>)
    }
    else {
      return (<div>{boxes}</div>)
    }
  }
}

Results.propTypes = {};

Results.stateToProps = (state) => ({
  // Original banner image for a fallback
  originalBannerImage: state.originalBannerImage,
  // Logged-in user TODO: Hardcoded user
  userId: state.userId,
  // Filter applied to search result
  filter: state.filter
})

Results.dispatchToProps = (dispatch) => bindActionCreators(actions, dispatch)

export default connect(Results.stateToProps, Results.dispatchToProps)(Results)





export default ({
  reset: (focus, orignalBannerImage) => ({
    type: 'STATE',
    state: {
      itemFocus: focus,
      itemGrew: true,
      bannerImage: orignalBannerImage
    }
  }),

  activeDonation: (focus, donationImage) => ({
    type: 'STATE',
    state: {
      itemFocus: focus,
      itemGrew: true,
      bannerImage: donationImage
    }
  }),

  activeDonationSet: (activeDonation) => ({
    type: 'STATE',
    state: {
      activeDonation: activeDonation
    }
  })
})

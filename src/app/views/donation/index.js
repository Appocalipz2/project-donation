import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actions from './actions'
import fire from '../../core-components/Fire/fire';
import style from './styles.scss'
import DonationBox from '../../components/DonationBox';
import DonationButton from '../../components/DonationButton';
import history from '../../core-components/History/history';
import moment from 'moment';
import request from 'request';

var initialSet = 0
var savedDonation = false

class Donation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fireBaseDonations: fire.database().ref('donations').limitToLast(100),
      description: [],
      bullets: [],
      topics: [],
      funding: [],
      donationActive: false,
      errorText: ""
    }
  }

  // Javascript version to get url parameters
  getParameterByName(name, url) {
    if (!url) url = window.location.href;
    var regex = new RegExp("[?&]" + (name.replace(/[\[\]]/g, "\\$&")) + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  // If the page contains no id then it will throw a 404 so rather than that we take the user to the home page
  invalidFilterAttribute () {
    if (typeof this.getParameterByName("id") === 'undefined' || this.getParameterByName("id") === '' || this.getParameterByName("id") === null || this.getParameterByName("id") === 'null'){
      history.push("/");
    }
  }

  componentWillMount () {
    this.invalidFilterAttribute()

    // Remove old connections
    this.state.fireBaseDonations.off()

    // Reset all information
    this.state.fireBaseDonations
      .orderByChild('id')
      .equalTo(this.getParameterByName("id"))
      .on('child_changed', (innerSnapshot) => {
        this.props.activeDonationSet(typeof innerSnapshot.val() === 'undefined' ? {} : innerSnapshot.val())
        this.setState({
          description: (typeof innerSnapshot.val().description === 'undefined' ? [] : innerSnapshot.val().description),
          bullets: (typeof innerSnapshot.val().bullets === 'undefined' ? [] : innerSnapshot.val().bullets),
          topics: (typeof innerSnapshot.val().topics === 'undefined' ? [] : innerSnapshot.val().topics),
          funding: (typeof innerSnapshot.val().funding === 'undefined' ? [] : innerSnapshot.val().funding)
        })
      })

    this.state.fireBaseDonations
      .orderByChild('id')
      .equalTo(this.getParameterByName("id"))
      .once('child_added', snapshot => {
        const refs = fire
          .database()
          .ref('donations')

        refs.orderByChild('id')
            .equalTo(this.getParameterByName("id"))
            .on('value', (innerSnapshot) => {
            if (initialSet === 0) {
              refs.off("value")
              innerSnapshot.forEach((weekSnapshot) => {
                weekSnapshot.ref.update({ views: snapshot.val().views + 1 });
              });
            }
            else {
              this.props.activeDonationSet(typeof innerSnapshot.val()[Object.keys(innerSnapshot.val())[0]] === 'undefined' ? {} : innerSnapshot.val()[Object.keys(innerSnapshot.val())[0]])
              this.setState({
                description: (typeof innerSnapshot.val()[Object.keys(innerSnapshot.val())[0]].description === 'undefined' ? [] : innerSnapshot.val()[Object.keys(innerSnapshot.val())[0]].description),
                bullets: (typeof innerSnapshot.val()[Object.keys(innerSnapshot.val())[0]].bullets === 'undefined' ? [] : innerSnapshot.val()[Object.keys(innerSnapshot.val())[0]].bullets),
                topics: (typeof innerSnapshot.val()[Object.keys(innerSnapshot.val())[0]].topics === 'undefined' ? [] : innerSnapshot.val()[Object.keys(innerSnapshot.val())[0]].topics),
                funding: (typeof innerSnapshot.val()[Object.keys(innerSnapshot.val())[0]].funding === 'undefined' ? [] : innerSnapshot.val()[Object.keys(innerSnapshot.val())[0]].funding)
              })
            }
          });

        this.props.activeDonationSet(typeof snapshot.val() === 'undefined' ? {} : snapshot.val())
        this.setState({
          description: (typeof snapshot.val().description === 'undefined' ? [] : snapshot.val().description),
          bullets: (typeof snapshot.val().bullets === 'undefined' ? [] : snapshot.val().bullets),
          topics: (typeof snapshot.val().topics === 'undefined' ? [] : snapshot.val().topics),
          funding: (typeof snapshot.val().funding === 'undefined' ? [] : snapshot.val().funding)
        })

        // Set the current main banner image to this items banner image
        this.props.activeDonation(true, `/assets/images/donations/${snapshot.val().banner}`)
    })
  }

  // Difference between days
  dayDiff (first, second) {
    return Math.round((second-first)/(1000*60*60*24));
  }
  // Difference between minutes
  minDiff (first, second) {
    return Math.floor((first - second) / 60000)
  }

  renderAbout () {
    return (
      <div className={style.aboutContent}>
        <div className={style.aboutContentInner}>
          {this.state.description.map((item, i) => <div key={i} className={style.descriptionItem}>{item}</div>)}
          <ul className={style.bullets}>
            {this.state.bullets.map((bullet, i) => <li key={`bullet-${i}`} className={style.bulletLi}>{bullet}</li>)}
          </ul>
          {this.state.topics.map((topic, i) => {
            if (typeof topic.image !== 'undefined' && topic.image !== '' && topic.image !== null) {
              return (
                <div key={i}>
                  <div style={{backgroundImage: `url('/assets/images/donations/${topic.image}')`}} className={style.topicImage}></div>
                  <div className={style.topicDescription}>{topic.description}</div>
                </div>
              )
            }
            else {
              return <div key={i}></div>
            }
          })}
          <div className={style.donateButtonContainer}>
            <div className={style.donateButton}  onClick={() => this.setState({donationActive: true})}>
              Donate
            </div>
          </div>
        </div>
      </div>
    )
  }

  getTime(timeA, timeB) {
    let totalTime = ""
    var days = this.dayDiff(moment(timeA, "YYYY-MM-DD'T'HH:mm:SS'Z'").toDate().getTime(), timeB)
    if (days === 0) {
      var minutes = this.minDiff(moment(timeA, "YYYY-MM-DD'T'HH:mm:SS'Z'").toDate().getTime(), timeB)
      if (minutes >= 60) totalTime = `${Math.abs(minutes / 60)} hours ago`
      else totalTime = `${Math.abs(minutes)} minutes ago`
    }
    else totalTime = `${Math.abs(days)} days ago`
    return totalTime
  }

  renderSupporters () {
    var ids = []
    let mainItem = ""
    var mainUser = this.state.funding.sort((a, b) => (moment(a.created).toDate().getTime() < moment(b.created).toDate().getTime() ? 1 : -1)).filter(fund => fund.userId === this.props.userId)[0]
    var otherUsers = this.state.funding.filter(fund => {
      if (fund.userId !== this.props.userId && ids.indexOf(fund.userId) === -1) {
        ids.push(fund.userId)
        return true
      }
      return false
    })

    if (typeof mainUser === 'undefined') {
      mainItem = (
        <div className={style.meContainer}>
          <div style={{backgroundImage: `url('${this.props.userImage}')`}} className={style.meImage}></div>
          <div className={style.noDonation}>You have not donated yet</div>
        </div>
      )
    }
    else {
      mainItem = (
        <div className={style.meContainer}>
          <div style={{backgroundImage: `url('${mainUser.profileImage}')`}} className={style.meImage}></div>
          <div className={style.name}>{mainUser.firstname} {mainUser.lastname}</div>
          <div className={style.when}>{this.getTime(mainUser.created, (new Date()).getTime())}</div>
        </div>
      )
    }

    return (
      <div>
        <div className={style.titleContainer}>
          <div className={style.titleInnerContainer}>Me</div>
        </div>
        {mainItem}
        <div className={style.titleContainer}>
          <div className={style.titleInnerContainer}>
            Other Supporters
          </div>
        </div>
        {otherUsers.map(other => {
          return (
            <div className={style.meContainer}>
              <div style={{backgroundImage: `url('${other.profileImage}')`}} className={style.meImage}></div>
              <div className={style.name}>{other.firstname} {other.lastname}</div>
              <div className={style.when}>{this.getTime(other.created, (new Date()).getTime())}</div>
            </div>
          )
        })}
        <div className={`${style.donateButtonContainerAlt} ${style.donateButtonContainer}`}>
          <div className={style.donateButton} onClick={() => this.setState({donationActive: true})}>
            Donate
          </div>
        </div>
      </div>
    )
 }

  renderContact () {
    return(<div className={style.contact}>
      No Contact Information Available
    </div>)
  }

  donationButtonCancel () {
    this.setState({donationActive: false})
  }

  donationButtonProceed (total) {
    if (total <= 0) {
      this.setState({errorText: "Invalid"})
    }
    else {
      var ref1 = fire.database().ref('count')
      ref1.on('value', (innerSnapshot) => {
        ref1.off("value")
        innerSnapshot.forEach((weekSnapshot) => {
          if (weekSnapshot.key.toLowerCase() === "donations") {
            console.log(weekSnapshot.val() + 1)
            ref1.update({ donations: weekSnapshot.val() + 1 });
          }
        })
      })

      var ref = fire.database().ref('donations')
      ref.equalTo(this.getParameterByName("id"))
        .orderByChild('id')
        .on('value', (innerSnapshot) => {
          innerSnapshot.forEach((weekSnapshot) => {
            // Reorder and fix array if index has shifted
            var fixedArray = []
            var array = (typeof weekSnapshot.val().funding === 'undefined' ? [] : weekSnapshot.val().funding);
            array.map(item => fixedArray.push(item))

            const entry = ({
              "donation": parseFloat(total),
              "firstname": "Melcom",
              "lastname": "van Eeden",
              "userId": "0000-0000-1000-1000",
              "profileImage": "http://www.humanlongevity.com/wp-content/themes/hli/assets/home/learn-more-health-nucleus.jpg",
              "created": moment().format("YYYY-MM-DDTHH:mm:SSZ")
            })

            fixedArray.push(entry)
            ref.off("value")
            weekSnapshot.ref.update({ funding: fixedArray });
          });
        });

      this.setState({donationActive: false})
    }
  }

  render() {
    this.invalidFilterAttribute()

    if (!this.props.itemFocus) {
      return (
        <div>
          <div className={style.loadingContent}>
            <div className={style.loadingContentText}>
              Loading Page ...
            </div>
          </div>
        </div>
      )
    }
    else {
      if(this.props.activeTab === 0) {
        return <div>{this.renderAbout ()} {(this.state.donationActive ? <DonationButton donate={(e) => {this.donationButtonProceed(e)}} cancel={() => {this.donationButtonCancel()}}></DonationButton> : null)}</div>
      }
      else if (this.props.activeTab === 1) {
        return <div>{this.renderSupporters ()} {(this.state.donationActive ? <DonationButton donate={(e) => {this.donationButtonProceed(e)}} cancel={() => {this.donationButtonCancel()}}></DonationButton> : null)}</div>
      }
      else {
        return <div>{this.renderContact ()} {(this.state.donationActive ? <DonationButton donate={(e) => {this.donationButtonProceed(e)}} cancel={() => {this.donationButtonCancel()}}></DonationButton> : null)}</div>
      }
    }
  }
}

Donation.propTypes = {};

Donation.stateToProps = (state) => ({
  itemFocus: state.itemFocus,
  activeTab: state.activeTab,
  userId: state.userId,
  userImage: state.userImage,
})

Donation.dispatchToProps = (dispatch) => bindActionCreators(actions, dispatch)

export default connect(Donation.stateToProps, Donation.dispatchToProps)(Donation)







import React from 'react';
import PropTypes from 'prop-types';
import Link from '../../core-components/Link';
import s from './styles.css';

class ErrorPage extends React.Component {
  static propTypes = {
    error: React.PropTypes.object,
  };

  componentDidMount() {
    document.title = this.props.error && this.props.error.status === 404 ?
      'Page Not Found' : 'Error';
  }

  goBack = (event) => {
    event.preventDefault();
    // history.goBack();
  };

  render() {
    return (
      <div></div>
    );
  }

}

export default ErrorPage;

import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import FastClick from 'fastclick';
import { Provider } from 'react-redux';
import store from './core-components/Store/store';
import router from './core-components/Router/router';
import history from './core-components/History/history';
import {persistStore, autoRehydrate} from 'redux-persist'
import firebase from 'firebase';
import style from './styles.scss'

import NavBar from './components/TopBar'
import BannerSection from './components/BannerSection'
import Navigation from './components/Navigation'

// Routes load
var routes = require('./../resources/routes.json').default;

// Preset style to the container
const container = document.getElementById('container');
container.style.width = "100%";
container.style.height = "100%";
container.style.float = "left";

// Important components
const persistor = persistStore(store)

//  createStore
const renderComponent = (component) => {
  var __componentProps = component.props
  var content = {
    ...component,
    props: {
      store: store,
      persistor: persistor,
      ...__componentProps
    }
  }

  ReactDOM.render(
    // Useless container so we just make it use the entire page
    <div style={{height: "100%", width: "100%"}}>
      <NavBar store={store} persistor={persistor}></NavBar>
      <BannerSection store={store} persistor={persistor}></BannerSection>
      <Navigation store={store} persistor={persistor}></Navigation>
      <Provider store={store} persistor={persistor}>
        {content}
      </Provider>
    </div>
    , container
  );
}


const render = (location) => {
  router.resolve(routes, location)
    .then(renderComponent)
    .catch(error => router.resolve(routes, { ...location, error }).then(renderComponent));
}

history.listen(render);
render(history.location);
FastClick.attach(document.body);

if (module.hot) {
  module.hot.accept('./../resources/routes.json', () => {
    routes = require('./../resources/routes.json').default;
    render(history.location);
  });
}
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'
import mainImage from '../../../www/img/main.jpg'

// Redux store logger
const logger = createLogger();

// Allow the initial state to only be set once
var setInitialStateOnce = false

// Global initial state object
const initialState = {
  activeTab: 0,
  itemFocus: false,
  itemGrew: false,

  donations: 0,

  filter: 'created',

  bannerImage: mainImage,
  originalBannerImage: mainImage,

  activeDonation: {},

  userId: "0000-0000-1000-1000",
  userImage: "http://www.humanlongevity.com/wp-content/themes/hli/assets/home/learn-more-health-nucleus.jpg"
}

// Create the redux store with a thunk and logger middleware
const store = createStore((state, action) => {
  // Set the initial state once
  if (!setInitialStateOnce) {
    setInitialStateOnce = true
    state = initialState;
  }

  // Reducers
  const newState = action.state
  switch (action.type) {
    case 'STATE':
      return { ...state, ...newState };

    default:
      return state;
  }
}, {}, applyMiddleware(thunk, logger));

export default store;

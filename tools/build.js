const fs = require(`fs`);
const rimraf = require(`rimraf`);
const ejs = require(`ejs`);
const webpack = require(`webpack`);

// const task = require(`./common/task`);
// const system = require(`eunomia-controller/variables`)('./src/resources/config.json');
// const html = require(`./generate/html`);
// const sitemap = require(`./generate/sitemap`);
// const humans = require(`./generate/humans`);
// const crossdomain = require(`./generate/crossdomain`);
// const browserconfig = require(`./generate/browserconfig`);

// Eunomia Controller - Personal common library and core of this deployment
const eunomia = require(`eunomia-controller`);
const task = eunomia.task;
const system = eunomia.variables('./src/resources/config.json');
const humans = eunomia.generate.humans("./public/");
const sitemap = eunomia.generate.sitemap("./public/");
const browserconfig = eunomia.generate.browserconfig("./public/");
const crossdomain = eunomia.generate.crossdomain("./public/");
const assets = eunomia.generate.assets("./public/", "./src/www/img");
const html = eunomia.generate.html;

// Bundle JavaScript, CSS and image files with Webpack
const bundle = task(`bundle`, (resolve, reject) => {
  const webpackConfig = require(`./config/webpack.config`);
  return new Promise((i, ii) => {
    webpack(webpackConfig).run((err, stats) => {
      if (err) {
        reject()
      } else {
        resolve();
      }
    });
  });
});

// Build website into a distributable format
module.exports = task(`build`, () => {
  global.DEBUG = process.argv.includes(`--debug`) || false;
  rimraf.sync(`public/*`, { nosort: true, dot: true });
  return Promise.resolve()
    .then(bundle)
    .then(html("./public/", "dist/", null, true))
    .then(humans)
    .then(sitemap)
    .then(browserconfig)
    .then(crossdomain)
    .then(assets)
});
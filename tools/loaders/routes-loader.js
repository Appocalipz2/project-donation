
const system = require(`eunomia-controller/variables`)('./src/resources/config.json');

const toRegExp = require('path-to-regexp');

function escape(text) {
  return text.replace('\'', '\\\'').replace('\\', '\\\\');
}

module.exports = function routesLoader(source) {
  this.cacheable();

  const output = ['[\n'];
  const routes = JSON.parse(source);


  for (const route of routes) {
    const path = `${__dirname}/../../${system.new.views.full}/${route.page}`;
    const keys = [];
    const pattern = toRegExp(route.path.split("?")[0], keys);
    const require = route.chunk && route.chunk === 'app' ?
      module => `Promise.resolve(require('${escape(module)}').default)` :
      module => `new Promise(function (resolve, reject) {
        try {
          require.ensure(['${escape(module)}'], function (require) {
            resolve(require('${escape(module)}').default);
          }${typeof route.chunk === 'string' ? `, '${escape(route.chunk)}'` : ''});
        } catch (err) {
          reject(err);
        }
      })`;
    output.push('  {\n');
    output.push(`    path: '${escape(route.path)}',\n`);
    if (route.navBarTitle) {
      output.push(`    navBarTitle: '${escape(route.navBarTitle)}',\n`);
    }


    if (route.state) {
      output.push(`    state: '${JSON.stringify(route.state)}',\n`);
    }
    if (route.group) {
      output.push(`    group: '${escape(route.group)}',\n`);
    }
    output.push(`    pattern: ${pattern.toString()},\n`);
    output.push(`    keys: ${JSON.stringify(keys)},\n`);
    output.push(`    page: '${escape(`${path}`)}',\n`);
    if (route.data) {
      output.push(`    data: ${JSON.stringify(route.data)},\n`);
    }
    output.push(`    load() {\n      return ${require(`${path}`)};\n    },\n`); // eslint-disable-line import/no-dynamic-require
    output.push('  },\n');
  }

  output.push(']');



  return `export default ${output.join('')};`;
};
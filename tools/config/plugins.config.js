const path = require('path');
const system = require(`eunomia-controller/variables`)('./src/resources/config.json');
const webpack = require('webpack');
const AssetsPlugin = require('assets-webpack-plugin');
const global = require('./global.config.js');

// Plugins used within this project
module.exports = [
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': global.isDebug ? '"development"' : '"production"',
    __DEV__: global.isDebug
  }),
  new AssetsPlugin({
    path: path.resolve(__dirname, `../../${system.path.dist.public}`),
    filename: 'assets.json',
    prettyPrint: global.isDebug
  }),
  new webpack.LoaderOptionsPlugin({
    debug: global.isDebug,
    minimize: !global.isDebug
  })
];




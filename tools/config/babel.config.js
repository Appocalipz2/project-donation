const pkg = require('../../package.json');
const global = require('./global.config.js');

// All babel related config
module.exports = Object.assign({}, pkg.babel, {
  babelrc: false,
  cacheDirectory: !!global.HMR,
  presets: pkg.babel.presets.map(x => x === 'latest' ? ['latest', { es2015: { modules: false } }] : x)
});

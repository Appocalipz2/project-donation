module.exports = [
  require('postcss-import')(),
  require('postcss-custom-properties')(),
  require('postcss-media-minmax')(),
  require('postcss-custom-selectors')(),
  require('postcss-calc')(),
  require('postcss-nesting')(),
  require('postcss-color-function')(),
  require('pleeease-filters')(),
  require('pixrem')(),
  require('postcss-selector-matches')(),
  require('postcss-selector-not')(),
  require('postcss-flexbugs-fixes')(),
  require('postcss-image-set-polyfill')(),
  require('postcss-short')(),
  require('cssnano')(),
  require('autoprefixer')()
]

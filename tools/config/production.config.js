const global = require('./global.config.js');
const webpack = require('webpack');

module.exports = (config) => {
  if (!global.isDebug) {
    config.plugins.push(new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: global.isVerbose
      }
    }));
    config.plugins.push(new webpack.optimize.AggressiveMergingPlugin());
  }
}
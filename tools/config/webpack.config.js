const path = require('path');
const global = require('./global.config.js');
const system = require(`eunomia-controller/variables`)('./src/resources/config.json');
const rules = require('./rules.config.js');
const plugins = require('./plugins.config.js');

const config = {
    context: path.resolve(__dirname, `../../${system.path.src.root}`),
    entry: [
        '!!style-loader!css-loader!react-mdl/extra/material.min.css',
        'react-mdl/extra/material.min.js',
        `./app/${system.file.root}`
    ],
    output: {
        path: path.resolve(__dirname, `../../${system.path.dist.public}`),
        publicPath: global.isDebug ? `http://localhost:${process.env.PORT || system.port}/${system.path.dist.root}/` : `/${system.path.dist.root}/`,
        filename: global.isDebug ? '[name].js?[hash]' : '[name].[hash].js',
        chunkFilename: global.isDebug ? '[id].js?[chunkhash]' : '[id].[chunkhash].js',
        sourcePrefix: '  '
    },
    devtool: global.isDebug ? 'source-map' : false,
    stats: {
        colors: true,
        reasons: global.isDebug,
        hash: global.isVerbose,
        version: global.isVerbose,
        timings: true,
        chunks: global.isVerbose,
        chunkModules: global.isVerbose,
        cached: global.isVerbose,
        cachedAssets: global.isVerbose
    },
    plugins,
    module: {
        rules
    },
  node: {
    console: true,
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  }
};

const production = require('./production.config.js')(config);
const development = require('./development.config.js')(config);

module.exports = config;
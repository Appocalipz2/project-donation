/*
 Global Env => Global Context
 */

module.exports = {
  useHMR: !!global.HMR,
  isDebug: global.DEBUG === false ? false : !process.argv.includes('--release'),
  isVerbose: process.argv.includes('--verbose') || process.argv.includes('-v')
};


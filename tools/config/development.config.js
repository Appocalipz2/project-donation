const global = require('./global.config.js');
const babelConfig = require('./babel.config.js');
const webpack = require('webpack');

// Functionality that should execute in a development env.
module.exports = (config) => {
  if (global.isDebug && global.useHMR) {
    babelConfig.plugins.unshift('react-hot-loader/babel');
    config.entry.unshift('react-hot-loader/patch', 'webpack-hot-middleware/client');
    config.plugins.push(new webpack.HotModuleReplacementPlugin());
    config.plugins.push(new webpack.NoEmitOnErrorsPlugin());
  }
}
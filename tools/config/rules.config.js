const path = require('path');
const system = require(`eunomia-controller/variables`)('./src/resources/config.json');
const global = require('./global.config.js');
const babelConfig = require('./babel.config.js');
const postcss = require('./postcss.config.js');

const coreCSS = [
  { loader: 'style-loader' },
  { loader: 'css-loader', options: {sourceMap: global.isDebug, importLoaders: true, modules: true, localIdentName: global.isDebug ? '[name]_[local]_[hash:base64:3]' : '[hash:base64:4]', minimize: !global.isDebug} },
  { loader: 'postcss-loader', options: { plugins: (loader) => postcss }}
]

module.exports = [
  { test: /\.tsx?$/, loader: 'ts-loader' },
  { test: /\.jsx?$/, loader: 'babel-loader', include: [path.resolve(__dirname, `../../${system.path.src.root}`), path.resolve(__dirname, `../../${system.new.components}`)], options: babelConfig },
  { test: /\.css$/, use: coreCSS },
  { test: /\.scss$/, use: [...coreCSS, { loader: "sass-loader" }]},
  { test: /\.less$/, use: [...coreCSS, { loader: "less-loader" }]},
  { test: /\.json$/, loader: 'json-loader', exclude: [ path.resolve(__dirname, `../../${system.new.resources.full}/routes.json`) ] },
  { test: /\.json$/, use: [{ loader: 'babel-loader', options: babelConfig }, { loader: path.resolve(__dirname, `./../loaders/routes-loader.js`)}], include: [ path.resolve(__dirname, `../../${system.new.resources.full}/routes.json`) ]},
  { test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)$/, loader: 'url-loader', options: { limit: 10000 } },
  { test: /\.(eot|ttf|wav|mp3)$/, loader: 'file-loader' }
];

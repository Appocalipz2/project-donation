
const rimraf = require(`rimraf`);
const webpack = require(`webpack`);
const bsync = require(`browser-sync`);

// Eunomia Controller - Personal common library and core of this deployment
const eunomia = require(`eunomia-controller`);
const task = eunomia.task;
const system = eunomia.variables('./src/resources/config.json');
const humans = eunomia.generate.humans("./public/");
const sitemap = eunomia.generate.sitemap("./public/");
const browserconfig = eunomia.generate.browserconfig("./public/");
const crossdomain = eunomia.generate.crossdomain("./public/");
const assets = eunomia.generate.assets("./public/", "./src/www/img");
const html = eunomia.generate.html;

// Hot Module Reload
global.HMR = !process.argv.includes(`--no-hmr`);

module.exports = task(`main`, () => new Promise((resolve, reject) => {
  // File system function
  rimraf.sync(`public/*`, {
    nosort: true,
    dot: true
  });

  // Allows this function to only run once in the hot reload state
  let count = 0;

  // Create the browser sync instance
  const bs = bsync.create();

  // Include the webpack.config on every reload
  const webpackConfig = require(`./config/webpack.config`);
  const compiler = webpack(webpackConfig);

  // Compiles application in watch mode - HMR
  const webpackDevMiddleware = require(`webpack-dev-middleware`)(compiler, {
    publicPath: webpackConfig.output.publicPath,
    stats: webpackConfig.stats,
    noInfo: true
  });

  // Add a plugin to webpack
  compiler.plugin(`done`, (stats) => {
    Promise.resolve()
      .then(html("./public/", "dist/", stats))
      .then(humans)
      .then(sitemap)
      .then(browserconfig)
      .then(crossdomain)
      .then(assets)
      .then(() => {
        count += 1;
        if (count === 1) {
          bs.init({
            port: (system.port) || 3000,
            ui: {
              port: Number(system.port || 3000) + 1
            },
            server: {
              baseDir: `${system.path.public.root}`,
              middleware: [
                webpackDevMiddleware,
                require(`webpack-hot-middleware`)(compiler),
                require(`connect-history-api-fallback`)()
              ]
            }
          }, resolve);
        }
      })
      .catch((reason) => {
        console.log(`Run failed, Reason: ${reason}`);
        reject();
      });
  });
}));
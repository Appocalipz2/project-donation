Live Preview (May be slow running on a free AWS EC2):
http://ec2-34-213-152-41.us-west-2.compute.amazonaws.com

## Initial install and dev server
npm install && yarn install && yarn start

## Initial install and build
npm install && yarn install && yarn run build

When deploying to a apache env. you will need a .htaccess file to be able to refresh the page with the redux routes.

## .htaccess
RewriteEngine On

RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]

RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d

RewriteRule ^ - [L]

RewriteRule ^ /index.html [L]

## Requirements:
- React, MithrilJS or Angular (1 or 4) SPA web app 
	- I decided to use ReactJS which can be ported to a ReactJS Native app if an app would be required.
- Responsive behaviour
	- Working
- Landing page with show/hide tab sections
	- Completed
- Campaign detail page with show/hide sections
	- Completed
- Functional donation to a specific campaign (i.e. can submit a donation and it will update the overall contribution and % of goal reached. design not provided)
	- Hooked up and working
- All data to be dynamic and live in Firebase (you can define the structure)
	- Hooked up and working
- Animations
	- Included

## Approach:

Below you can find the steps I took to achieve a end product.

- Database design and structure
- Common lib - basic setup (This is my personal lib called eunomia, eunomia basically manages your website structure, https://bitbucket.org/Appocalipz2/project-donation-eunomia-controller)
- Project setup (Where the project will live and be designed in. This would be the ReactJS project. This is also my personal structure which I'm building up to be the almost perfect reactjs structure.)
- Initial page and routes setup
- Page design and basic CSS
- Bug testing and flow
- Hooked up firebase
- Second Bug testing and flow
- Improved CSS and added animations
- Third bugs and missing features testings
- Production env setup and deployment
- Test the live peview

## Notes:
Currently I'm hosting the web app on a free EC2 server (which is really-really slow). In a real-time usage env. I would rather use AWS Lambda, Beanstalk, S3 for static files with a firebase database

## Notes about Eunomia - In-Progress
Eunomia refers to the goddess Eunomia (https://en.wikipedia.org/wiki/Eunomia_(goddess)) - Eunomia was the goddess of law and legislation and one of the Second Generation of the Horae along with her sisters Dikē and Eirene. The Horae were law and order goddesses who maintained the stability of society, and were worshipped primarily in the cities of Athens, Argos and Olympia. From Pindar:

How this fits in is that my lib eunomia controlls the reactjs structure in the dev and building phases.

Eunomia creates your index.html, sitemap, brwoserconfig, humans, crossdomain configs and your assets.

Eunomia also contains a task manaher and a json structure controller (also my personal script which allows a little bit of logic in your json for example reusing field within the json field, globals and javascript computations)

This lib is fairly new and is still under heavy construction. Using it within this donation project helped me to futher progress in creating the perfect structure controller. (Will be more dynamic in the future)

## Notes about my ReactJS Structure - In-Progress
I'm trying to create the perfect reactjs structure which does css, js && img pre and post processing, contains graphs, database connections, aws, hot reloading with redux routes, min structure (controlled by eunomia), unit testing etc.

It should reduce the dev time in CSS and JS and allow elegant and all around awesome structure.

Also In-progress, started with this a few weeks ago and will take time and testing to perfect





